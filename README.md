## API REST DESTINADA PARA CALCULAR FRETE
---
Está API REST serve para fazer cálculo de frete de acordo com parâmetros de Origem e Destino.
A aplicação trabalha com dados em memória (H2) e consome dados da API do Google para saber a real distância entre duas cidades.

---

## Tecnologias usadas

1. Java 8.
2. Spring Boot v.2.1.6.
3. Spring Boot data JPA
4. Spring Boot devtools.
5. H2 database.
6. Swagger. 
7. Unirest.
8. Gson.


---

## Como executar a aplicação

Primeiro é necessário executar o clone deste repositório, após isso, faça os seguintes passos:

1. Instale o **Apache Maven** e configure a variável de Ambiente no seu PATH.
2. Execute no terminal ou Prompt de Comando **mvn exec:java**.
3. Na primeira vez pode demorar um pouco, pois estará fazendo o download das libs. 

---

## Documentação da API

 Com a aplicação iniciada, acesse pelo seguinte endereço **http://localhost:9000/swagger-ui.html**
 Obs: Só poderá acessar com a aplicação rodando.

