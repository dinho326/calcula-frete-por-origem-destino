/* insert transportador */

insert into transportador (name, custo_por_quilo, limitador_custo_por_quilo_km)
values ('vsc cargo',0.2, 1000);

insert into transportador (name, custo_por_quilo, limitador_custo_por_quilo_km)
values ('btclog',0.08, 1100);

insert into transportador (name, custo_por_quilo, limitador_custo_por_quilo_km)
values ('dugrao',0.06, 1800);

insert into transportador (name, custo_por_quilo, limitador_custo_por_quilo_km) 
values ('fm transp',0.12, 1160);

insert into transportador (name, custo_por_quilo, limitador_custo_por_quilo_km) 
values ('rodogami',0.16, 1230);

insert into transportador (name, custo_por_quilo, limitador_custo_por_quilo_km) 
values ('coocatrans',0.17, 1060);


/* insert contatos */
insert into contato(nome, sobre_nome, email, telefone, transportador_id)
values('alfredo','silva','alfredo@gmail.com','(21) 99995-9200', 1);


insert into contato(nome, sobre_nome, email, telefone, transportador_id)
values('patricia','gomes','patricia@gmail.com','(21) 96999-0000', 2);

insert into contato(nome, sobre_nome, email, telefone, transportador_id)
values('edilson','carvalho','dinho@gmail.com','(21) 96915-9200', 3);

insert into contato(nome, sobre_nome, email, telefone, transportador_id)
values('maicon','santana','maicon@gmail.com','(21) 91111-4562', 4);

insert into contato(nome, sobre_nome, email, telefone, transportador_id)
values('leandro','shurer','leandro@gmail.com','(21) 96915-9200', 5); 


/* insert endereco */
insert into endereco (cidade, estado, transportador_id) values ('duque de caxias', 'rj', 1);