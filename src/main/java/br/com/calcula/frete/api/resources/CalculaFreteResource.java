package br.com.calcula.frete.api.resources;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.calcula.frete.api.service.GoogleAPI;
import br.com.calcula.frete.api.service.TransportadorService;
import br.com.calcula.frete.api.util.Constantes;
import br.com.calcula.frete.api.util.Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Classe controller responsável por enviar o calculo na 
 * resposta da requisição
 * @author Edilson Carvalho
 * @since 02/07/2019
 */

@Api(value="API REST Calcula Frete")
@Controller
@RequestMapping("/calcula-frete")
public class CalculaFreteResource {
	
	@Autowired
	private TransportadorService transportadorService;
	
	@ApiOperation(value = "Recupera informações de Frete")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Fretes Calculados")
    })
	@RequestMapping(value="/{origins}/{destinations}",method=RequestMethod.GET)
	public ResponseEntity<?> getTransportador(@PathVariable("origins") String origins,
			@PathVariable("destinations") String destinations) {
		
		CacheControl cacheControl = CacheControl.maxAge(30, TimeUnit.SECONDS);
		GoogleAPI googleAPI = new GoogleAPI(origins, destinations);
		if("OK".equalsIgnoreCase(googleAPI.getStatusElements())) {
			return ResponseEntity.status(HttpStatus.OK).cacheControl(cacheControl).body(
					Util.insereValorCustoQuiloCarga(googleAPI.getKm(), transportadorService.getListaTransportador()));
			
		}
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).cacheControl(cacheControl).body(Constantes.GOOGLE_NOT_FOUND);
	}
}
