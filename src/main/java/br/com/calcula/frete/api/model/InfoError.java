package br.com.calcula.frete.api.model;

/**
 * Classe modelo para resposta de erros na aplicação
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
public class InfoError {

	private String title;
	private Long status;
	private Long timestamp;
	private String message;
	
	
	
	public InfoError(String title, Long status, Long timestamp, String message) {
		super();
		this.title = title;
		this.status = status;
		this.timestamp = timestamp;
		this.message = message;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
