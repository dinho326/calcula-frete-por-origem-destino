package br.com.calcula.frete.api.util;

import java.util.ArrayList;

/**
 * Classe responsável por ser espelho
 * do objeto Json da API do google
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
public class GoogleAPIDistance {

	  private ArrayList<Object> destination_addresses = new ArrayList<Object>();
	  private ArrayList<Object> origin_addresses = new ArrayList<Object>();
	  private ArrayList<Object> rows = new ArrayList<Object>();
	  private String status;

	  public String getStatus() {
	    return status;
	  }
	  public void setStatus( String status ) {
	    this.status = status;
	  }
	public ArrayList<Object> getDestinationAddresses() {
		return destination_addresses;
	}
	public void setDestinationAddresses(ArrayList<Object> destinationAddresses) {
		this.destination_addresses = destinationAddresses;
	}
	public ArrayList<Object> getOriginAddresses() {
		return origin_addresses;
	}
	public void setOriginAddresses(ArrayList<Object> originAddresses) {
		this.origin_addresses = originAddresses;
	}
	public ArrayList<Object> getRows() {
		return rows;
	}
	public void setRows(ArrayList<Object> rows) {
		this.rows = rows;
	}
}
