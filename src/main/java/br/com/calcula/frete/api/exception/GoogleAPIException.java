package br.com.calcula.frete.api.exception;

/**
 * Classe que representa uma exceção
 * ao utilizar o serviço do google
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
public class GoogleAPIException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GoogleAPIException(String mensagem) {
		super(mensagem);
	}

	public GoogleAPIException(String mensagem, Throwable error) {
		super(mensagem, error);
	}
}
