package br.com.calcula.frete.api.exception;

/**
 * Classe que representa um NotFound de transportador
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
public class TransportadorNotFound extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransportadorNotFound(String mensagem) {
		super(mensagem);
	}

	public TransportadorNotFound(String mensagem, Throwable error) {
		super(mensagem, error);
	}
}
