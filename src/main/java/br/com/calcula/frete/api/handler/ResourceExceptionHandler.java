package br.com.calcula.frete.api.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import br.com.calcula.frete.api.model.InfoError;
import br.com.calcula.frete.api.util.Constantes;
import br.com.calcula.frete.api.exception.GoogleAPIException;
import br.com.calcula.frete.api.exception.TransportadorNotFound;

/**
 * Classe responsável por ouvir exceções e tratar resposta
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
@ControllerAdvice
public class ResourceExceptionHandler {

	
	@ExceptionHandler(TransportadorNotFound.class)
	public ResponseEntity<InfoError> handleTransportadorNotFound
	(TransportadorNotFound e, HttpServletRequest request){
		
		InfoError erro = new InfoError(
				Constantes.TRANSPORTADOR_NOT_FOUND, 404l, System.currentTimeMillis(), Constantes.MESSAGE_ERROR);
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
	
	
	@ExceptionHandler(GoogleAPIException.class)
	public ResponseEntity<InfoError> handleGoogleAPIException
	(GoogleAPIException e, HttpServletRequest request){
		
		InfoError erro = new InfoError(
				Constantes.GOOGLE_EXCEPTION, 404l, System.currentTimeMillis(), Constantes.MESSAGE_ERROR);
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
	
}
