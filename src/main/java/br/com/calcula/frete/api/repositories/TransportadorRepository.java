package br.com.calcula.frete.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.calcula.frete.api.model.Transportador;

/**
 * Interface que estende de JpaRepository
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
public interface TransportadorRepository extends JpaRepository<Transportador, Long> {

}
