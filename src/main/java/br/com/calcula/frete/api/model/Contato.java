package br.com.calcula.frete.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Classe Modelo de contato
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
@Entity
public class Contato {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Size(max=70)
	private String nome;
	
	@NotNull
	@Size(max=70)
	private String sobreNome;
	
	@NotNull
	@Email
	@Size(max=35)
	private String email;
	
	@NotNull
	private String telefone;
	
	@OneToOne
	 @MapsId
	 @JsonIgnore
	 private Transportador transportador;
	 
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getSobreNome() {
		return sobreNome;
	}


	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public Transportador getTransportador() {
		return transportador;
	}


	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
}
