package br.com.calcula.frete.api.resources;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.calcula.frete.api.model.Transportador;
import br.com.calcula.frete.api.service.TransportadorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Classe controller que apresenta informações
 * do transportador na resposta da requisição
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
@Api(value="API REST Transportador")
@Controller
@RequestMapping("/api/transportador")
public class TransportadorResource {
	
	@Autowired
	private TransportadorService transportadorService;
	
	@ApiOperation(value = "Recupera um Transportador cadastrado")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Transportador encontrado")
    })
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public ResponseEntity<?> getTransportador(@PathVariable("id") Long id) {
		
		CacheControl cacheControl = CacheControl.maxAge(30, TimeUnit.SECONDS);
		Transportador transportador = transportadorService.getTransportadorById(id);
		return ResponseEntity.status(HttpStatus.OK).cacheControl(cacheControl).body(transportador);
	}
	
	@ApiOperation(value = "Recupera uma lista de Transportadores cadastrados")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Transportadores encontrados")
    })
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<?> getListaTransportador() {
		
		CacheControl cacheControl = CacheControl.maxAge(30, TimeUnit.SECONDS);
		return ResponseEntity.status(HttpStatus.OK).cacheControl(cacheControl).body(transportadorService.getListaTransportador());
	}

}
