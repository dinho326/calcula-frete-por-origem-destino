package br.com.calcula.frete.api.util;

import java.util.List;

import br.com.calcula.frete.api.model.Transportador;

/**
 * Classe Utilitária para todo o projeto
 * 
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
public class Util {

	public static List<Transportador> insereValorCustoQuiloCarga(Long km, List<Transportador> listaTransportador) {

		for (Transportador transportador : listaTransportador) {
			
			if (km > transportador.getLimitadorCustoPorQuiloKm()) {
				
				Long limiteCustoTotal = transportador.getLimitadorCustoPorQuiloKm();
				double custoPorQuilo = transportador.getCustoPorQuilo();
				
				Long resto =  km - limiteCustoTotal ;
				
				double percentAumento = ((double)resto / limiteCustoTotal) * 100;

				double novoCustoPorQuilo = custoPorQuilo * (percentAumento / 100) + custoPorQuilo;

				transportador.setCustoTotalPorQuilo(novoCustoPorQuilo);
				continue;
			}
				transportador.setCustoTotalPorQuilo(transportador.getCustoPorQuilo());
		}

		return listaTransportador;
	}
}
