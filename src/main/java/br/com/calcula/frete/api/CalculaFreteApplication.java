package br.com.calcula.frete.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Classe de inicialização do projeto
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
@SpringBootApplication
public class CalculaFreteApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculaFreteApplication.class, args);
	}

}
