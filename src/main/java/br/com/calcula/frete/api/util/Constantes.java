package br.com.calcula.frete.api.util;

/**
 * Classe que contém Constantes
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
public class Constantes {

	public final static String GOOGLE_EXCEPTION = "Exceção ao tentar consumir serviço do Google";
	public final static String GOOGLE_NOT_FOUND = "Não encontramos resultados";
	public final static String TRANSPORTADOR_NOT_FOUND = "Transportador não encontrado";
	public final static String MESSAGE_ERROR = "Maiores informações entre em contato com dinho326@gmail.com";
	public final static String TITLE_API = "Frete API REST";
	public final static String DESCRICAO_API = "API REST para consulta de calculo de frete";
	public final static String VERSAO = "1.0";
	public final static String TERMOS_OF_SERVICOS = "Termos do serviço";
	public final static String LICENSA = "Apache License Version 2.0";
	public final static String LICENSA_URL = "http://www.apache.org/licenses/LICENSE-2.0.txt";
	public final static String NOME_CONTATO = "Edilson Carvalho";
	public final static String URL_CONTATO = "https://www.linkedin.com/in/edilson-carvalho-80275493";
	public final static String EMAIL_CONTATO = "dinho326@gmail.com";
	
	
}
