package br.com.calcula.frete.api.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Classe Modelo que representa informações básicas de Transportador
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
@Entity
public class Transportador {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String name;
	@NotNull
	private Double custoPorQuilo;
	
	@NotNull
	@JsonProperty("Limitador do custo por quilo em Km")
	private Long limitadorCustoPorQuiloKm;
	
	@Transient
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("Custo Total Por Quilo")
	private Double custoTotalPorQuilo;
	
	@OneToMany(mappedBy = "transportador", cascade = CascadeType.ALL)
    private List<Endereco> enderecos;

	@OneToOne(mappedBy = "transportador", cascade = CascadeType.ALL)
	private Contato contato;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public Double getCustoPorQuilo() {
		return custoPorQuilo;
	}

	public void setCustoPorQuilo(Double custoPorQuilo) {
		this.custoPorQuilo = custoPorQuilo;
	}

	public Long getLimitadorCustoPorQuiloKm() {
		return limitadorCustoPorQuiloKm;
	}

	public void setLimitadorCustoPorQuiloKm(Long limitadorCustoPorQuiloKm) {
		this.limitadorCustoPorQuiloKm = limitadorCustoPorQuiloKm;
	}

	public Double getCustoTotalPorQuilo() {
		return custoTotalPorQuilo;
	}

	public void setCustoTotalPorQuilo(Double custoTotalPorQuilo) {
		this.custoTotalPorQuilo = custoTotalPorQuilo;
	}

}
