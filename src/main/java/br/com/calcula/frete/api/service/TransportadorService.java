package br.com.calcula.frete.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.calcula.frete.api.exception.TransportadorNotFound;
import br.com.calcula.frete.api.model.Transportador;
import br.com.calcula.frete.api.repositories.TransportadorRepository;
import br.com.calcula.frete.api.util.Constantes;
/**
 * Classe Service responsável por gerenciar 
 * as consultas/inserções na base de dados
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
@Service
public class TransportadorService  {
	
	@Autowired
	private TransportadorRepository transportadorRepository;
	
	
	public Transportador getTransportadorById(Long id) {
		
		Transportador transportador = transportadorRepository.findById(id).orElse(null);
		
		if(transportador == null) {
			throw new TransportadorNotFound(Constantes.TRANSPORTADOR_NOT_FOUND);
		}
		return transportador;
	}
	
	public List<Transportador> getListaTransportador(){
		
		return transportadorRepository.findAll();
	}

}