package br.com.calcula.frete.api.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.com.calcula.frete.api.exception.GoogleAPIException;
import br.com.calcula.frete.api.util.GoogleAPIDistance;

/**
 * Classe responsável por gerenciar as consultas
 * na API do Google
 * @author Edilson Carvalho
 * @since 02/07/2019
 */
public class GoogleAPI {

	private static final String KEY = "key";
	private static final String API_KEY = "AIzaSyAIqalKrB3sptXgf6R6WPRY9a11NDdqiwM";
	private static final String BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json";
	private static final String[] DEFAULT_KEYS = {"units","origins","destinations"};
	private static final String DEFAULT_VALUE = "metric";
	private static final String STATUS_OK = "OK";
	private Long km;
	private String statusGlobal;
	private String statusElements;
	
	public GoogleAPI(String origins, String destinations ) {
		getResponseBody(origins,destinations);
	}
	
	public  String getResponseBody(String origins, String destinations) {
		
		try {
			String json = Unirest.get(BASE_URL)
			.queryString(DEFAULT_KEYS[0], DEFAULT_VALUE)
			.queryString(DEFAULT_KEYS[1], origins)
			.queryString(DEFAULT_KEYS[2], destinations)
			.queryString(KEY, API_KEY)
			.asString()
			.getBody();
			
			validaResponse(json);
			return json;
		} catch (UnirestException e) {
			throw new GoogleAPIException(e.getMessage());
		}
		
	}
	
	private  void validaResponse(String json) {
		Gson gson = new Gson();
		GoogleAPIDistance googleAPIDistance = gson.fromJson(json, GoogleAPIDistance.class);
		this.statusGlobal = googleAPIDistance.getStatus();
		if(STATUS_OK.equals(googleAPIDistance.getStatus())) {
			setKM(json);
		}
	}

	private void setKM(String json) {
		this.km = 0L;
		try {
			final JSONObject jo = new JSONObject(json);
		    JSONArray arrays = jo.getJSONArray("rows");
		    JSONObject rows = arrays.getJSONObject(0);

		    JSONArray elemnts = rows.getJSONArray("elements");
		    JSONObject distanceDurantion = elemnts.getJSONObject(0);
		    
		    this.statusElements = (String) distanceDurantion.get("status");
		    if(STATUS_OK.equals(distanceDurantion.get("status"))) {
		    	JSONObject distanciaObitida = distanceDurantion.getJSONObject("distance");
		    	String distancia = (String) distanciaObitida.get("text");
		    	this.km = Long.parseLong(distancia.replaceAll("[^0-9]*",""));
		    }
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	

	public Long getKm() {
		return km;
	}

	public void setKm(Long km) {
		this.km = km;
	}

	public String getStatusGlobal() {
		return statusGlobal;
	}

	public void setStatusGlobal(String statusGlobal) {
		this.statusGlobal = statusGlobal;
	}

	public String getStatusElements() {
		return statusElements;
	}

	public void setStatusElements(String statusElements) {
		this.statusElements = statusElements;
	}
	
}
